#************************************************************************************
#Networking things
#************************************************************************************
resource "aws_network_interface" "int_hdst" {
  subnet_id              = aws_subnet.app_subnet.id
  security_groups        = [aws_security_group.ssh-open.id]
  attachment {
    instance     = aws_instance.ec2_hdst.id
    device_index = 1
  }
  tags = {
    Name                 = "HolaLuz-HDST-MVP"
  }
depends_on = [
  aws_instance.ec2_hdst,
  aws_security_group.ssh-open,
  ]
}
#************************************************************************************
#Generamos la key
#************************************************************************************

resource "tls_private_key" "instance" {
  algorithm             = "RSA"
  rsa_bits              = 4096
}

resource "aws_key_pair" "instance" {
  key_name              = var.ec2_name
  public_key            = tls_private_key.instance.public_key_openssh
}

output "ssh-key-instance" {
  value                 = tls_private_key.instance.private_key_pem
  sensitive             = true
}
#*************************************************************************************
#Security Group cfg
#*************************************************************************************

resource "aws_security_group" "ssh-open" {
  name        = var.ec2_name
  vpc_id      = aws_vpc.app_vpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "HolaLuz-HDST-MVP"
  }
depends_on = [
  aws_vpc.app_vpc,
]
}

#*************************************************************************************
#EC2 Instance
#*************************************************************************************
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}

resource "aws_instance" "ec2_hdst" {
  ami                         = data.aws_ami.ubuntu
  instance_type               = var.instance_type
  vpc_security_group_ids      = [aws_security_group.ssh-open.id]
  subnet_id                   = aws_subnet.subnet_hdst.id
  key_name                    = aws_key_pair.instance.key_name


  network_interface {
    network_interface_id = aws_network_interface.int_hdst.id
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "unlimited"
  }
depends_on = [
  aws_security_group.ssh-open,
  aws_subnet.subnet_hdst,
  aws_key_pair.instance,
  aws_network_interface.int_hdst,
]
}

#*************************************************************************************
#DNS
#*************************************************************************************

resource "aws_route53_zone" "selected" {
  name                 = format("%s%s",var.ec2_zone_name,var.zone_name)

  tags = {
    Environment = "app hdst"
  }
}

resource "aws_route53_record" "instance" {
  zone_id             = aws_route53_zone.selected.zone_id
  name                = var.api_name
  type                = "A"
  ttl                 = "60"
  records             = [aws_network_interface.int_hdst.private_ip]
depends_on = [
  aws_network_interface.int_hdst,
]
}