#************************************************************************************
#CREATE VPC
#************************************************************************************
resource "aws_vpc" "app_vpc" {
    cidr_block              = var.vpc_cidr

    name                    = format("%s%s",var.api_name,"-app-vpc")


    enable_dns_support      = “true” 
    enable_dns_hostnames    = “true”
    enable_classiclink      = “false”
    instance_tenancy        = “default”    
    
    tags {
        Name = “app_vpc”
    }
}

resource “aws_subnet” “app_subnet” {
    vpc_id                  = aws_vpc.app_vpc.id
    cidr_block              = var.ec2_subnet_cidr
    map_public_ip_on_launch = true
    availability_zone       = format("%s%s",var.ec2_zone_name,"a")
    tags {
        Name = “app_subnet”
    }
depends_on = [
    aws_vpc.app_vpc,
]
}

resource "aws_internet_gateway" "app_ig" {
  vpc_id                    = aws_vpc.app_vpc.id
depends_on = [
    aws_vpc.app_vpc,
]
}

resource "aws_route_table" "app_rt" {
  vpc_id                   = aws_vpc.app_vpc.id

  route {
      cidr_block     = "0.0.0.0/0"
      gateway_id     = aws_internet_gateway.app_ig.id
    }
  }
depends_on = [
    aws_vpc.app_vpc,
    aws_internet_gateway.app_ig,
]
}

resource "aws_route_table_association" "app_ta"{
    subnet_id               = aws_subnet.app_subnet.id
    route_table_id          = aws_route_table.app_rt.id
depends_on = [
    aws_subnet.app_subnet,
    aws_route_table.app_rt,
]
}
