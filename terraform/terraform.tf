terraform {
    backend "s3" {
        bucket = "holaluz-terraform-tfstate"
        encrypt = true
        key = "hdst-instance/state"
        region = "us-west-1"
        profile = var.profile
    }
}