variable "db_name" {
  type = string
  description = "RDS Name"
}
variable "zone_name" {
  type = string
  description = "the AWS Zone Name"
}
variable "ec2_zone_name" {
  type = string
  description = "the AWS EC2 Zone Name"
}
variable "ec2_name" {
  type = string
  description = "EC2 Name"
}
variable "api_name" {
  type = string
  description = "DNS API Name"
}
variable "profile" {
  type = string
  description = "AWS Profile Selection"
}
variable "instance_type" {
  type = string
  description = "AWS EC2 Instance Type"
}
variable "db_instance_type" {
  type = string
  description = "DataBase Instance Type"
}
variable "vpc_cidr" {
  type = string
  description = "VPC CIDR"
}
variable "db_subnet_cidr" {
  type = string
  description = "DB Subnet CIDR"
}
variable "ec2_subnet_cidr" {
  type = string
  description = "EC2 Subnet CIDR"
}