#************************************************************************************
#CREATE subnet
#************************************************************************************
resource "aws_subnet" "rds" {
  vpc_id                  = aws_vpc.app_vpc.id
  cidr_block              = var.db_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone       = format("%s%s",var.ec2_zone_name,"a")
  tags {
    Name = "psql_subnet"
  }
}
resource "aws_db_subnet_group" "psql_sg" {
  name                   = format("%s%s",var.api_name,"-psql-sng")
  subnet_ids             = [aws_subnet.rds.id]
}



#************************************************************************************
#CREATE SG and PSW
#************************************************************************************
resource "random_string" "psql_password" {
  length  = 32
  upper   = true
  number  = true
  special = false
}

resource "aws_security_group" "psql_sg" {
  vpc_id      = aws_vpc.app_vpc.id
  name        = format("%s%s",var.api_name,"-psql-sg")
  description = "Allow all inbound for Postgres"
ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.app_subnet.id]
  }
depends_on = [
    aws_vpc.app_vpc,
    aws_subnet.subnet_hdst,
]
}

#************************************************************************************
#CREATE RDS PSQL
#************************************************************************************
resource "aws_db_instance" "psql_db" {
  identifier             = format("%s%s",var.api_name,"-psql-db")
  name                   = format("%s%s",var.api_name,"-psql-db")
  instance_class         = var.db_instance_type
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "12.5"
  skip_final_snapshot    = true
  publicly_accessible    = true
  db_subnet_group_name   = aws_db_subnet_group.psql_sng.id
  vpc_security_group_ids = [aws_security_group.psql_sg.id]
  username               = "holaluzdbadmin"
  password               = random_string.password.result
depends_on = [
    aws_security_group.psql_sg,
]
}