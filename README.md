# terraformHolaLuz

Solving this...

Exercise 1.
EC2 - TCP 80 API.
Aprovisionamiento (opcional) - Ansible

-- notas:
Definimos la subnet 172.16.10.0/24 para las instancias que seran de esta aplicacion.

Suponiendo que no es un RDS EC2-Classic. Utilizaremos un data para traernos la informacion.

En el terraform del RDS con su tfstate tenemos que agregar al security group la direccion de nuestra ec2 + puerto para permitir el ingreso.

RDS name: rds-hdst

Suponemos hay creado un bucket para tfstate: holaluz-terraform-tfstate


Terraform v1.2.9
on linux_amd64


Ansible Playbook:
ansible-playbook -i inventory vue_js.yml

# Apartado Questions

## Question 1:
Imagine that this project is critical for Holaluz. Describe in terms of "cloud provider elements" the
architecture to achieve High Availability with this application. To give you some hints, think about how to
avoid issues if a server/container fails, to allow deployments without outages, and keep it simple .

To generate HA in a EC2 instances you need to consider 3 factors.
 . Loads Trafics increase --> Auto Scaling Groups
 . auto scaling --> multiple machinnes --> Elastic Load Balancing
 . For multizone HA --> Availability Zones (auto scaling group)

I mean then, for an HA structure we need the EC2 Instances to have an Auto Scaling Group to increase the number of instances when needed. For the Auto Scaling Group to be effective we need to apply an Elastic Load Balancer to redistribute traffic across instances. In turn, all this has to work in Multi AZ so as not to depend on a single area where it can fail.

## Question 2:
Could you broadly describe what you would use to automate the Continuous Delivery of this application?
It's unnecessary to submit code details, but make sure you explain the reasoning behind the actions you
suggest.
To give you some hints, think about: How does code from your computer make its way to be available to
final users? How could we reduce outages when pushing to production? How could we validate that
everything is working as expected?

Having two possibilities to solve it, depending on whether we deploy in EC2 or ECS, we are going to take into account the following.
For EC2:
To deploy the application in EC2 we are going to use the solution of mounting the Vue.js build as a unit in the instance. We will achieve this from the pipeline where with the build we will generate an artifact that we will later pass to an S3.
Once in S3 we will use s3fs to mount it on the instance and access the data. The volume must be added to the instance's fstab for it to support instance reboot.
To publish the EC2 instance we have to configure the internet access by network, also the Security Group rules to allow access from the outside. After that we are going to configure a DNS in Route53 to name the site.

For ECS:
To deploy in ECS we can directly generate a tagged image. This will contain all the build and its configuration to turn on when the container starts.
To carry out this task in the pipeline we are going to generate the build of the application that we will put inside the container along with the configuration (Dockerfile) and then we will generate the image, put a tag on it and upload it to a registry to be able to consume it later. ECS will grab the image from the registry and build a container from it in Fargate Cluster.
We generate the tag and container configuration, select the correct VPN and Subnet, configure the inbound rules in security group and configure DNS with route53.

To validate that everything is working, we are going to review the healtcheck with http 200, which must previously be configured by the developers. This check will be part of our monitoring to have the site monitored.

# Miscellaneous:

# 1 - During your first day at Holaluz, you have been working on platform code but, by mistake, you have been adding commits to git master branch, not on a feature branch, how can you "erase" the local master commit but maintain the code changes to move them to a new branch?

If the commits are pushed to master we need to "revert" this changes. To do this first create a branch with the changes "git checkout feature/branch" and "git merge master" after that go back to master, execute "git log" to see the commits and select the last before your changes. Now execute "git revert branchcode", do this with all commits. Push again to master and it's all. 
This can also be done by "git reset --hard HEAD~3" 3 is the numbers of commit to go back. Anyway I recommend revert, it's more secure.

# 2 - We have a massive PostgreSQL cluster in RDS with millions of rows in some tables. Suddenly, one query starts showing up in the Slow Query Log. Please describe the actions you would do to diagnose and fix this issue.

To diagnose I would have metrics with alerts in a monitoring system (like datadog, grafana u others) which would alert me against a slow load peaks. I would identify the query and notify to the corresponding team to resolve it.
To colect more information we can use EXPLAIN in the query. If the query can be stopped we will stop their with "SELECT pg_cancel_backend(pid)" or if the query cannot be killed "SELECT pg_terminate_backend(pid)".
If for any reason we don't have the pid we can use "SELECT pid FROM pg_stat_activity WHERE (now() - pg_stat_activity.query_start) > interval '5 minutes';" in '5 minutes' we can write the seconds/minutes to find this query pid.

# 3 - A team is starting a new project, and they are discussing which CI/CD system fits best. Some team members had proposed GitHub Actions and some others a self-hosted Jenkins. They are not reaching a consensus, so they ask you to help them to decide. What would you do?

To define which CI/CD you need to use, you have to take into account where it is currently hosted. Assuming that we have our repositories on GitHub, we can say that GitHub Actions has the advantage over Jenkins since the configuration is minimal. This is convenient for getting started quickly. If our code is not on GitHub we will definitely go for Jenkins.
So if we have our code on GitHub we still have things to consider. GitHub Actions has a cost while Jenkins is Open Source. Jenkins is more complex but more versatile.
Depending on the stage of the company, and if your repos are on GitHub, GitHub Actions would be a good option to get up and running quickly. If we are looking for a more robust approach, we should opt for Jenkins.

# 4 - It's Friday evening, and the core production system is down. Everyone is stressed, and a couple of devs are firing suggestions at you. You are the the most experienced platform engineer available to fix this issue, what would you do?

First I calm everyone down a bit, second I check the monitoring system to see the magnitud of the problem. Then I Check logs for any clue. When I have all the information I make a plan to solve the incident inmediatly. If the problem cannot be resolved quickly, I contact my superior and notify him, I also notify everyone via slack so that they are aware.
Problems of thyrt party, like cloud down, can't be resolved by my. I take the case and generate the issues in the providers and I follow up.
If I can't resolve the problem I also contact my superior and notify him, after that I investigate the issue to find the best option to be resolved.
After all I make a report for the issue to documentation of incident.